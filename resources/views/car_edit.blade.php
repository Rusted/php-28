@extends('layout')

@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        Pataisykite raudonai pažymėtus laukus.
    </div>
@endif
<form action="{{ route('car_update', ['id' => $car->id]) }}" method="POST">
<div class="form-group @if ($errors->has('reg_number')) has-error @endif">
    <label class="col-md-2 control-label">Reg. numeris</label>
    <div class="col-md-10">
        @if ($errors->has('reg_number'))
        <small class="text-danger">
            {{$errors->first('reg_number')}}
        </small>
        @endif
        <input type="text" name="reg_number" class="form-control" placeholder="Reg. numeris" value="{{ Request::old('reg_number') ?  Request::old('reg_number'): $car->reg_number }}">
    </div>
</div>

<div class="form-group @if ($errors->has('model')) has-error @endif">
    <label class="col-md-2 control-label">Modelis</label>
    <div class="col-md-10">
        @if ($errors->has('model'))
        <small class="text-danger">
            {{$errors->first('model')}}
        </small>
        @endif
        <input type="text" name="model" class="form-control" placeholder="" value="{{ Request::old('model') ?  Request::old('model'): $car->model }}">
    </div>
</div>

<div class="form-group @if ($errors->has('brand')) has-error @endif">
    <label class="col-md-2 control-label">Gamintojas</label>
    <div class="col-md-10">
        @if ($errors->has('brand'))
        <small class="text-danger">
            {{$errors->first('brand')}}
        </small>
        @endif
        <input type="text" name="brand" class="form-control" placeholder="" value="{{ Request::old('brand') ?  Request::old('brand'): $car->brand }}">
    </div>
</div>
{{csrf_field()}}
<input type="submit">
</form>
@endsection