@extends ('layout')

@section('content')
<ul>
@foreach ($documents as $document)
    <li>
        <a href="{{ route('document_download', ['id' => $document->id]) }}">
            {{ $document->original_name }}
        </a>
    </li>
@endforeach
</ul>
@endsection
