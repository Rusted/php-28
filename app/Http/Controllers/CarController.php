<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CarUpdateRequest;
use App\Car;
class CarController extends Controller
{
    public function edit($id, Request $request)
    {
        return view('car_edit', ['car' => Car::find($id)]);
    }

    public function update($id, CarUpdateRequest $request)
    {
        $car = Car::find($id);
        $car->model = $request->input('model');
        $car->reg_number = $request->input('reg_number');
        $car->brand = $request->input('brand');
        
        $car->save();
        
        return \Redirect::back()->withSuccess( "Atnaujinta sėkmingai" );
    }
}
