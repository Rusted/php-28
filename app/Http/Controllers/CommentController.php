<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    public function view($id, Request $request)
    {
        return view('comment_view', ['comment' => Comment::find($id)]);
    }
}
