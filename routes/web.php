<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']], function () {
    Route::get('/owner/list', 'OwnerController@list');
    Route::get('comment/{id}', 'CommentController@view');
});

Route::group(['middleware' => ['admin']], function () {
    Route::get('car/{id}/edit', 'CarController@edit')->name('car_edit');
    Route::post('car/{id}/update', 'CarController@update')->name('car_update');
});

Route::post('language/change', 'LanguageController@change')->name('language_change');

Route::resource('education', 'EducationController');

Route::get('document/form', 'DocumentController@form')->name('document_form');
Route::get('document/list', 'DocumentController@list')->name('document_list');
Route::get('document/{id}/download', 'DocumentController@download')->name('document_download');

Route::post('document/upload', 'DocumentController@upload')->name('document_upload');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
